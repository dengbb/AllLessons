//
//  SetRequest.h
//  MoveCar
//
//  Created by dengbb on 15/3/25.
//
//

#import <Foundation/Foundation.h>
#import "NetWorkModel.h"

@interface SetRequest : NSObject<NSURLConnectionDelegate, NSURLConnectionDataDelegate>

+(SetRequest*)sharedInstance;

-(void )postRequestWithURL: (NSString *)url postParems:(NSMutableDictionary *)postParems content:(void(^)(NetWorkModel *model))requestResult;

@end
