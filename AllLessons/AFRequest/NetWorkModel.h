//
//  NetWorkModel.h
//  MoveCar
//
//  Created by dengbb on 15/3/25.
//
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperation.h"
@interface NetWorkModel : NSObject

@property (nonatomic, assign) NSInteger statusCode;
@property (nonatomic, strong) NSArray * responeArray;
@property (nonatomic, strong) NSDictionary *responseDic;
@property (nonatomic, strong) NSString *responseStr;
@property (nonatomic, strong) NSError * error;
@property (nonatomic, strong) AFHTTPRequestOperation *operation;

@end
