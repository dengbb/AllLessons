//
//  SetRequest.m
//  MoveCar
//
//  Created by dengbb on 15/3/25.
//
//

#import "SetRequest.h"
#import "AFNetworking.h"


@implementation SetRequest

+(SetRequest*)sharedInstance{
    
    static dispatch_once_t onceToken;
    static SetRequest* sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SetRequest alloc]init];
    });
    
    return sharedInstance;
    
}

-(void )postRequestWithURL: (NSString *)url postParems:(NSMutableDictionary *)postParems content:(void(^)(NetWorkModel *model))requestResult
{
    
    
    AFHTTPRequestOperationManager *m = [AFHTTPRequestOperationManager manager];
    
    
    m.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    m.responseSerializer = [AFHTTPResponseSerializer serializer];
    [m POST:url parameters:postParems success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NetWorkModel* model =[[NetWorkModel alloc]init];
        model.responseDic = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:NSJSONReadingMutableContainers error:nil];
        model.responeArray = [NSJSONSerialization JSONObjectWithData:[operation responseData] options:NSJSONReadingMutableContainers error:nil];
        
        // 学生看
//        NSError *err;
//        [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:&err];
        requestResult(model);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NetWorkModel* model =[[NetWorkModel alloc]init];
        model.error = error;
        requestResult(model);
    }];
    
}


@end
