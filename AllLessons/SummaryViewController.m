//
//  SummaryViewController.m
//  AllLessons
//
//  Created by dengbb on 15/8/20.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import "SummaryViewController.h"
#import "SummaryTableViewCell.h"
#import "DPNViewController.h"
#import "MaoPaoViewController.h"
#import "UIImageView+AFNetworking.h"
#import "SwitchViewController.h"
#import "CodeViewController.h"
#import "CocoPodsViewController.h"
#import "UserNetWorkViewController.h"
#import "GCDViewController.h"
#import "CollectionViewController.h"
//#import "CollectionViewViewController.h"
#import "RefreshTableViewController.h"
#import "MJRefresh.h"
#import "SecondViewController.h"
#import "HandJumpViewController.h"
#import "FaceToFaceViewController.h"
#import "AutoLayoutViewController.h"
#import "Swipe.h"

@interface SummaryViewController ()

@end

@implementation SummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataSource = [[NSMutableArray alloc] initWithArray:@[@"委托,代理与通知",@"冒泡排序",@"第三方类库管理工具cocoaPods",@"三目运算符与宏,UserDefault,全局断点",@"手写JSON",@"ViewController生命周期与全局变量",@"页面跳转与手写UIBtton",@"OC之for循环",@"UILabel换行显示文字",@"选择语句",@"代码规范",@"使用网络",@"GCD",@"collection",@"collection2",@"Refresh",@"面试",@"Auto Layout"]];
    
    
    __weak UITableView *tableView = self.tableVIew;
    
    // 下拉刷新
    tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // 结束刷新
            [tableView.header endRefreshing];
        });
    }];
    
    // 设置自动切换透明度(在导航栏下面自动隐藏）
    tableView.header.automaticallyChangeAlpha= YES;
    
    // 上拉刷新
    tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // 结束刷新
            [tableView.footer endRefreshing];
        });
    }];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btn setTitle:@"编辑" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = right;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)click{
    self.tableVIew.editing = !self.tableVIew.editing;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataSource.count;
}
-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 147;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SummaryTableViewCell"];
    NSString *str = [_dataSource objectAtIndex:indexPath.row];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SummaryTableViewCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell.imagePng setImageWithURL:[NSURL URLWithString:@"http://pic1.nipic.com/2008-09-08/200898163242920_2.jpg"] placeholderImage:nil];
    cell.titleLabel.text = str;
    
    Swipe *swipeRecognizer = [[Swipe alloc] init];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    swipeRecognizer.idx = indexPath;
    [swipeRecognizer addTarget:self action:@selector(delete:)];
    [cell addGestureRecognizer:swipeRecognizer];
    
    
    
    return cell;
}
-(void)delete:(Swipe *)swip{
    
    [self.tableVIew beginUpdates];

    [self.tableVIew deleteRowsAtIndexPaths:@[swip.idx] withRowAnimation:UITableViewRowAnimationLeft];
    [_dataSource removeObjectAtIndex:swip.idx.row];

    [self.tableVIew endUpdates];
    
}

//当table进入编辑模式时，回调该方法获取应该是哪种编辑模式：插入，删除，none(移动)。模式不实现系统是使用删除模式
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*
     typedef NS_ENUM(NSInteger, UITableViewCellEditingStyle) {
     UITableViewCellEditingStyleNone,
     UITableViewCellEditingStyleDelete,
     UITableViewCellEditingStyleInsert
     };
     */
    return UITableViewCellEditingStyleDelete;
}
//当出现删除按钮时，回调该方法显示删除按钮的名字，默认不实现是delete
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(3_0){
    return @"删除";
}



//将要出现删除按钮时的回调，调整subview的位置
-(void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    
   
}

//删除按钮消失后的回调，用于重新调整subview到原来位置
-(void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    
  
    
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

//如果有关cell的edit的回调都不实现，只实现该回调，那么cell会使用默认行为，但手指滑动cell时会自动出现删除按钮，点击删除按钮会回调该方法进行数据删除
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [_dataSource removeObjectAtIndex:indexPath.row];
    //删除行并使用动画效果，并且该方法内部会自动调用reloaddata，刷新table的，不需要再使用reloaddata
    /*
     typedef NS_ENUM(NSInteger, UITableViewRowAnimation) {
     UITableViewRowAnimationFade,
     UITableViewRowAnimationRight,           // slide in from right (or out to right)
     UITableViewRowAnimationLeft,
     UITableViewRowAnimationTop,
     UITableViewRowAnimationBottom,
     UITableViewRowAnimationNone,            // available in iOS 3.0
     UITableViewRowAnimationMiddle,          // available in iOS 3.2.  attempts to keep cell centered in the space it will/did occupy
     UITableViewRowAnimationAutomatic = 100  // available in iOS 5.0.  chooses an appropriate animation style for you
     };
     */
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section]] withRowAnimation:UITableViewRowAnimationTop];
    //也可以使用reloaddata，不带动画效果的删除
    //[tableView reloadData];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    tableView.editing = !tableView.editing;
    return;
    
//    [_dataSource removeObjectAtIndex:indexPath.row];
//    [self.tableVIew reloadData];
    
    
    
//    [tableView beginUpdates];
//    
//    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
//    [_dataSource removeObjectAtIndex:indexPath.row];
//    
//    [self.tableVIew endUpdates];
//    
//    return;
    // 跳转方法
    if (indexPath.row == 0) {
        
        [self.navigationController pushViewController:[[DPNViewController alloc]init] animated:YES];
    }
    // 跳转方法
    if (indexPath.row == 1) {
        
        [self.navigationController pushViewController:[[MaoPaoViewController alloc]init] animated:YES];
    }
    // 跳转方法
    if (indexPath.row == 2) {
        
        [self.navigationController pushViewController:[[CocoPodsViewController alloc]init] animated:YES];
    }
    
    
    // 跳转方法 与手写控件
    if (indexPath.row == 5) {
        
        [self.navigationController pushViewController:[[SecondViewController alloc]init] animated:YES];
    }
    
    // 跳转方法 与手写控件
    if (indexPath.row == 6) {
        
        [self.navigationController pushViewController:[[HandJumpViewController alloc]init] animated:YES];
    }
    
    // 跳转方法
    if (indexPath.row == 9) {
        SwitchViewController *vc =  [[SwitchViewController alloc]init];
        vc.title = @"选择语句";
        vc.str = @"";
        [vc setStr:@""];
        [self.navigationController pushViewController:vc animated:YES];
    }
    // 跳转方法
    if (indexPath.row == 10) {
        CodeViewController *vc =  [[CodeViewController alloc]init];
        vc.title = @"选择语句";
        [self.navigationController pushViewController:vc animated:YES];
    }// 跳转方法
    if (indexPath.row == 11) {
        UserNetWorkViewController *vc =  [[UserNetWorkViewController alloc]init];
        vc.title = @"使用网络";
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row == 12) {
        GCDViewController *vc =  [[GCDViewController alloc]init];
        vc.title = @"GCD";
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row == 13) {
//        CollectionViewViewController *vc =  [[CollectionViewViewController alloc]init];
//        vc.title = @"Collection";
//        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row == 14) {
        CollectionViewController *vc =  [[CollectionViewController alloc]init];
        vc.title = @"Collection";
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row == 15) {
        RefreshTableViewController *vc =  [[RefreshTableViewController alloc]init];
        vc.title = @"Collection";
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    if (indexPath.row == 16) {
        FaceToFaceViewController *vc =  [[FaceToFaceViewController alloc]init];
        vc.title = @"面试";
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    if (indexPath.row == 17) {
        AutoLayoutViewController *vc =  [[AutoLayoutViewController alloc]init];
        vc.title = @"面试";
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}
@end
