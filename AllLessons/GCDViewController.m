//
//  GCDViewController.m
//  AllLessons
//
//  Created by dengbb on 15/8/24.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import "GCDViewController.h"

@interface GCDViewController ()

@end

@implementation GCDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _scrollView.contentSize = CGSizeMake(320*3, 50);
    
    
    /*
     
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     NSURL * url = [NSURL URLWithString:@"http://www.baidu.com"];
     NSError * error;
     NSString * data = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
     if (data != nil) {
     dispatch_async(dispatch_get_main_queue(), ^{
     NSLog(@"call back, the data is: %@", data);
     });
     } else {
     NSLog(@"error when download:%@", error);
     }
     });
     */
    

    // async 异步 queue 线程
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURL *url = [NSURL URLWithString:@"http://pic1.nipic.com/2008-09-08/200898163242920_2.jpg"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *img = [UIImage imageWithData:data];


        dispatch_async(dispatch_get_main_queue(), ^{
            // 更新UI
            _imageViewWithGCD.image = img;

        });
        
    });
    
    
}

-(void)GCD{
    
    
    //  后台执行：
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        // 耗时的操作
       

        
        // 主线程执行：
        dispatch_async(dispatch_get_main_queue(), ^{
            // something
            
        });
        
    });
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
