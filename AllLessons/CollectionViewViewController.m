//
//  CollectionViewViewController.m
//  AllLessons
//
//  Created by dengbb on 15/8/25.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import "CollectionViewViewController.h"
#import "CollectionViewViewCell.h"

@interface CollectionViewViewController ()

@end

@implementation CollectionViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataSource = @[
                    @{
                        @"headerTitle": @"i am a header",
                        @"footerTitle": @"i am a footer",
                        @"data": @[
                                @{
                                    @"title": @"cellTitle",
                                    @"content": @"cellcontent"
                                    }
                                ]
                        }
                    ];
    [self.collectionView registerClass:[CollectionViewViewCell class] forCellWithReuseIdentifier:@"CollectionViewViewCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSDictionary *dic = [_dataSource objectAtIndex:section];
    NSArray *arr = dic[@"data"];
    return arr.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (CollectionViewViewCelll *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *identifer = @"CollectionViewViewCelll";
    CollectionViewViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifer forIndexPath:indexPath];
    
    NSDictionary *dic = [_dataSource objectAtIndex:indexPath.section];
    NSArray *arr = dic[@"data"];
    NSDictionary *smallDic = [arr objectAtIndex:indexPath.row];
    cell.textLabel.text = smallDic[@"title"];
    cell.backgroundColor = [UIColor colorWithRed:((10 * indexPath.row) / 255.0) green:((20 * indexPath.row)/255.0) blue:((30 * indexPath.row)/255.0) alpha:1.0f];
    return cell;
}

#pragma mark --UICollectionViewDelegateFlowLayout
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(96, 100);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

#pragma mark --UICollectionViewDelegate
//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell * cell = (UICollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
}
//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return NO;
    }
    return YES;
}

@end
