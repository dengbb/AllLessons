//
//  HandJumpViewController.m
//  AllLessons
//
//  Created by dengbb on 15/10/27.
//  Copyright © 2015年 mohekeji. All rights reserved.
//

#import "HandJumpViewController.h"
#import "MaoPaoViewController.h"
@interface HandJumpViewController ()

@end

@implementation HandJumpViewController


#pragma -mark vc生命周期
- (void)viewDidLoad {
    [super viewDidLoad];

//    UIButton *btn = [[UIButton alloc] init];
//    btn.frame = CGRectMake(0, 0, 10, 10);
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(50, 100, 50, 50)];
    [btn setTitle:@"跳转" forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor purpleColor];
    
    [btn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btn];
    
    
    
    
    NSString *user =  [[NSUserDefaults standardUserDefaults] valueForKey:@"key"];
    NSLog(@"%@",user);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark 点击事件 clickEvent
-(void)click{

    /* 导航栏分两部分
    1.状态栏 22 包括 信号 时间 和 电池
    2.导航栏 44 px
    */
    // present 之后有导航栏
    UINavigationController *navc = [[UINavigationController alloc] initWithRootViewController:[[MaoPaoViewController alloc] init]];
    
//    [self.navigationController presentViewController:navc animated:YES completion:nil];
    
     [self.navigationController presentViewController:navc animated:YES completion:^{
         NSLog(@"页面已经消失");
     }];
}

@end
