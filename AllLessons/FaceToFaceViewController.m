//
//  FaceToFaceViewController.m
//  AllLessons
//
//  Created by dengbb on 15/10/27.
//  Copyright © 2015年 mohekeji. All rights reserved.
//

#import "FaceToFaceViewController.h"

@interface FaceToFaceViewController ()

@end

@implementation FaceToFaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
     5.对于语句NSString*obj = [[NSData alloc] init]; obj在编译时和运行时分别时什么类型的对象？
     
     编译时是NSString的类型；运行时是NSData类型的对象
     */
    
    NSString*obj = [[NSData alloc] init];
    // Do any additional setup after loading the view from its nib.
    
    
    
    /*
     7.id 声明的对象有什么特性？
     
     Id 声明的对象具有运行时的特性，即可以指向任意类型的objcetive-c的对象；
     */
    
    id name = @"my name";
    
    id arr = @[@"a",@"b",@"c"];

    NSLog(@"%@%@",name,arr);
    
    
    /*
     8.Objective-C如何对内存管理的,说说你的看法和解决方法?
     
     Objective-C的内存管理主要有三种方式ARC（自动内存计数）、手动内存计数、内存池。
     */
    
    
    /*
     13.描述一下iOS SDK中如何实现MVC的开发模式
     
     model
     view
     controller
     
     数据交互
     
     MVC是模型、试图、控制开发模式，对于iOS SDK，所有的View都是视图层的，它应该独立于模型层，由视图控制层来控制。所有的用户数据都是模型层，它应该独立于视图。所有的ViewController都是控制层，由它负责控制视图，访问模型数据。
     
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
