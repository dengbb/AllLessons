//
//  DPNViewController.h
//  AllLessons
//
//  Created by dengbb on 15/8/20.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DPNViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *delegateOriginLabel;
@property (strong, nonatomic) IBOutlet UILabel *notificationOrigionLabel;

@end
