//
//  SecondViewController.h
//  AllLessons
//
//  Created by dengbb on 15/10/22.
//  Copyright © 2015年 mohekeji. All rights reserved.
//


typedef enum role {
    teacher = 0,
    student

}Role;
#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property(nonatomic,strong)IBOutlet UIButton *nextPageBtn;
@property(nonatomic)Role role;
@end
