//
//  CollectionViewCell.h
//  AllLessons
//
//  Created by dengbb on 15/8/25.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property(nonatomic,strong)IBOutlet UIImageView *imgv;
@end
