//
//  DPN2ViewController.h
//  AllLessons
//
//  Created by dengbb on 15/8/20.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import <UIKit/UIKit.h>


/*
 【第二个页面】
 1在第二个页面写协议，写在interface 上面
 
 2.在第二个页面 实例化协议的变量
 
 3.让协议变量去做做协议中的方法
 
 【第一个页面】
 1.跳转页面的时候，签合同。
 
 vc2.delegate = self;   self为vc1
 
 2.在interface中实现这个协议
 
 3.在.m中实现协议方法。
 */

@protocol BuyIphone6sDelegate <NSObject>

-(void)buyIphone:(NSString *)str;

@end


@interface DPN2ViewController : UIViewController

@property(nonatomic,strong)id<BuyIphone6sDelegate>delegate;
@end
