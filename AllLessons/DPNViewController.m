//
//  DPNViewController.m
//  AllLessons
//
//  Created by dengbb on 15/8/20.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import "DPNViewController.h"
#import "DPN2ViewController.h"

@interface DPNViewController ()<BuyIphone6sDelegate>

@end

@implementation DPNViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
     第一个页面
     1.注册通知
     2.拿到暗号，做事情
     
     
     暗号和方法名
     第二个页面
     1：对暗号
     */
    // 注册通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(justDoIt:) name:@"changeLabel" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)justDoIt:(NSNotification *)obj{

    NSDictionary *dic = [obj object];
    _notificationOrigionLabel.text = dic[@"boom"];
}
- (IBAction)toNextPage:(id)sender {
    
   DPN2ViewController* vc =  [[DPN2ViewController alloc]init];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)buyIphone:(NSString *)str{
    
    _delegateOriginLabel.text = str;
}
@end
