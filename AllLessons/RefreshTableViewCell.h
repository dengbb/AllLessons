//
//  RefreshTableViewCell.h
//  AllLessons
//
//  Created by dengbb on 15/8/28.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RefreshTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *refreshLabel;

@end
