//
//  DPN2ViewController.m
//  AllLessons
//
//  Created by dengbb on 15/8/20.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import "DPN2ViewController.h"

@interface DPN2ViewController ()<UIAlertViewDelegate>

@end

@implementation DPN2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)delegateOrNotification:(id)sender {
   

    UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"提示" message:@"请选择" preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:ac animated:YES completion:^{
        
    }];
    
    
    UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"通知" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSDictionary * dic = @{@"boom":@"💣"};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeLabel" object:dic];
        
    }];
    [ac addAction:cancle];
    
    
    UIAlertAction *enter = [UIAlertAction actionWithTitle:@"委托" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [_delegate buyIphone:@"📱"];
        }];
    
    
    [ac addAction:enter];
    {
       
    }
    
    
    
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请选择设计模式" delegate:self cancelButtonTitle:@"委托" otherButtonTitles:@"通知", nil];
//    [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        // 委托
        [_delegate buyIphone:@"📱"];
    }else if (buttonIndex == 1){
    // 通知
        NSDictionary *dic = @{@"boom":@"💣"};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeLabel" object:dic];
        
    }
}
@end
