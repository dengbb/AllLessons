//
//  GCDViewController.h
//  AllLessons
//
//  Created by dengbb on 15/8/24.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GCDViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *imageViewWithGCD;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) NSArray *dataSource;
@end
