//
//  MaoPaoViewController.h
//  AllLessons
//
//  Created by dengbb on 15/8/20.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaoPaoViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *sortPre;
@property (strong, nonatomic) IBOutlet UILabel *sortAfter;
@property(nonatomic,strong)NSMutableArray *sortData;
@end
