//
//  ThirdViewController.m
//  AllLessons
//
//  Created by dengbb on 15/10/22.
//  Copyright © 2015年 mohekeji. All rights reserved.
//

#import "ThirdViewController.h"

@interface ThirdViewController ()

@end

@implementation ThirdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _valueLabel.text = _valueStr;
    
    _valueDicLabel.text = [_valueDic description];
    _valueArrLabel.text = [_valueArr description];
    [self setDic];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"%@",_valueArr);
    
}

-(void)setDic{

    // 这是个局部变量，在方法内实例化
    NSDictionary *dic = @{@"key":@"刚刚"};
    NSLog(@"%@",dic);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
