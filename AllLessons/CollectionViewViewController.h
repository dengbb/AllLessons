//
//  CollectionViewViewController.h
//  AllLessons
//
//  Created by dengbb on 15/8/25.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewViewController : UIViewController<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property(strong,nonatomic)NSArray *dataSource;
@end
