//
//  CollectionViewController.h
//  AllLessons
//
//  Created by dengbb on 15/8/25.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) IBOutlet UICollectionView *collection;

@property(nonatomic)NSArray *dataSource;
@end
