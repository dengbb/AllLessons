//
//  UserNetWorkViewController.m
//  AllLessons
//
//  Created by dengbb on 15/8/21.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import "UserNetWorkViewController.h"
#import "SetRequest.h"
#define URL(url) [NSString stringWithFormat:@"%@%@",V_SERVER_IP,url]
//获取祝福语列表
#define getBlessingMegList @"app=postcard&act=blessing_list_get"
#define V_SERVER_IP @"http://duoduo.fnchi.com/api.php?"  //test server
#import "MBProgressHUD.h"
#import "MJRefresh.h"
#import "UIImageView+AFNetworking.h"   // catgroy
@interface UserNetWorkViewController ()

@end

@implementation UserNetWorkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
     同步：是在主线程上运行
     异步：不是
     */
    
    
    //     2.使用AFNetworking 或SDWebImage 加载uiiamge
    // placeholderImage
    [_imageView setImageWithURL:[NSURL URLWithString:@"http://pic1.nipic.com/2008-09-08/200898163242920_2.jpg"] placeholderImage:[UIImage imageNamed:@"本地图片名字"]];
    
    
    
    /*
     加载图片
     1. 原生加载uiimage
     2.使用AFNetworking 或SDWebImage 加载uiiamge
     */

    // 数据源必须初始化，否则放不到里面数据
    _dataSource = [[NSMutableArray alloc] init];
    
    // 加载祝福语
    [self chooseBlessingMeg];

    // async 异步 queue 线程
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURL *url = [NSURL URLWithString:@"http://pic1.nipic.com/2008-09-08/200898163242920_2.jpg"];
        // 下载图片
        NSData *data = [NSData dataWithContentsOfURL:url];
        // data to image
        UIImage *img = [UIImage imageWithData:data];
        // != 为不等于
        if (img != nil) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _imageView.image = img;

            });
        }
        
    });
    
    // 上拉刷新 下拉加载
        __weak UITableView *tableView = self.tableView;
    tableView.header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        _pageNum  = 0;
        // pageNo 置为0
        [self chooseBlessingMeg];
    }];
    
    // 设置自动切换透明度(在导航栏下面自动隐藏)
    tableView.header.automaticallyChangeAlpha = YES;
    
    // 上拉刷新
    tableView.footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        // pageNo 加1  pageNo++
                _pageNum++;
        [self chooseBlessingMeg];
        
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)chooseBlessingMeg
{
    // 等待指示器 显示
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:[NSString stringWithFormat:@"%d",_pageNum] forKey:@"pageNo"];
    
    // 请求条件：1.url(接口名) 2.参数
    [[SetRequest sharedInstance] postRequestWithURL:URL(getBlessingMegList) postParems:dic content:^(NetWorkModel *model) {
        // 等待指示器 消失
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if(model.error){
            
            // 请求出错
            NSLog(@"%@",model.error);
            
        }else{
            
            NSLog(@"%@",model.responseDic);
            // 接数据的时候需要
            // 如果pageNo 是0 数据源清空，重新实例化即可
            if (_pageNum == 0) {
             _dataSource = [[NSMutableArray array]init];
                _dataSource = model.responseDic[@"data"];
            }else{
                // 如果pageNo 不是0 数据源清空
                [_dataSource addObjectsFromArray:model.responseDic[@"data"]];
                _dataSource = model.responseDic[@"data"];
            }
           
            [_tableView.header endRefreshing];
            [_tableView.footer endRefreshing];
            [_tableView reloadData];
    }
    }];
}



#pragma -mark TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataSource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSDictionary *dic = [_dataSource objectAtIndex:indexPath.row];
    cell.textLabel.text = dic[@"content"];
    
    return cell;
    
}


#pragma -mark Demo
@end
