//
//  ThirdViewController.h
//  AllLessons
//
//  Created by dengbb on 15/10/22.
//  Copyright © 2015年 mohekeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThirdViewController : UIViewController

/* 
 一下六个是全局变量
 */


@property(nonatomic,strong)IBOutlet UILabel* valueLabel;
@property(nonatomic,strong)IBOutlet UILabel* valueDicLabel;
@property(nonatomic,strong)IBOutlet UILabel* valueArrLabel;

@property(nonatomic,strong) NSString* valueStr;
@property(nonatomic,strong) NSDictionary* valueDic;
@property(nonatomic,strong) NSArray* valueArr;
@end
