//
//  CodeViewController.m
//  AllLessons
//
//  Created by dengbb on 15/8/21.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import "CodeViewController.h"

@interface CodeViewController ()

@end

@implementation CodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"代码规范";
    /*
     1.变量命名 可以使用数字 字母 下划线 （数字不可以开头）
     不可以使用oc中的预留变量 例如 blcok nsstring if
     变量 命名为驼峰式 例如  orderStatusLabel
     
     2.方法名 同 变量名 用逻辑业务命名
     
     3.类名：所有单词首字母都大写 例如： CodeViewController
     
     4.注释
     变量注释：
     单行注释 //例如对一个变量进行描述
     
     // 订单状态
     orderStatus = @"未出行";

     // 多行注释
     
     方法注释一般为多行注释
     方法名的含义，参数的含义
     */
    
    _codeLabel.text = @"1.变量命名可以使用数字字母下划线（数字不可以开头）不可以使用oc中的预留变量例如blcoknsstringif变量命名为驼峰式例如orderStatusLabel \n 2.方法名同变量名 \n3.类名：所有单词首字母都大写例如：CodeViewController \n4.注释变量注释：单行注释//例如对一个变量进行描述//订单状态orderStatus=@\"未出行\";      \n // 多行注释            方法注释一般为多行注释      方法名的含义，参数的含义";
    // 方法名为 login: passWord
    [self login:@"" passWord:@""];
    
    

}

/*
 登录方法
 userName:用户名
 passWord:用户密码
 */

-(void)login:(NSString *)userName passWord:(NSString *)passWord{

    // 需要学会写方法
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)savePeople:(NSString *)userName{

    
}
-(NSString *)savePeople:(NSString *)userName age:(NSString *)age sex:(NSString *)sex{

    return @"";
}

@end
