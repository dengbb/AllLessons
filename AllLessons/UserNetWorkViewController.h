//
//  UserNetWorkViewController.h
//  AllLessons
//
//  Created by dengbb on 15/8/21.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserNetWorkViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property(strong,nonatomic)NSMutableArray *dataSource;

@property(nonatomic)int pageNum;

@end
