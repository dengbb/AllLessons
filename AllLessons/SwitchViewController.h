//
//  SwitchViewController.h
//  AllLessons
//
//  Created by dengbb on 15/8/21.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *statusValue;
@property (strong, nonatomic) IBOutlet UILabel *statusNameLabel;
@property (strong, nonatomic)  NSString *str;
@end
