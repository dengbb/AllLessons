//
//  MaoPaoViewController.m
//  AllLessons
//
//  Created by dengbb on 15/8/20.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import "MaoPaoViewController.h"

@interface MaoPaoViewController ()

@end

@implementation MaoPaoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _sortData  = [[NSMutableArray alloc] initWithObjects:@"33",@"43",@"54",@"654",@"32",@"76",@"1",@"76", nil];
    _sortPre.text = [_sortData description];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)smallPao:(id)sender {
    
    
    for (int i = 0; i<_sortData.count; i++) {
        
        for (int j = i+1; j<_sortData.count; j++) {

            NSString *one = _sortData[i];
            NSString *next = _sortData[j];
            if ([one intValue] > [next intValue]) {
                _sortData[i] = next;
                _sortData[j] = one;
            }
            
            NSLog(@"数组：%@",_sortData);
        }
        NSLog(@"数组：%@",_sortData);
        
    }
    
    _sortAfter.text = [_sortData description];
}
- (IBAction)bigPao:(id)sender {
    
    _sortData  = [[NSMutableArray alloc] initWithObjects:@"33",@"43",@"54",@"654",@"32",@"76",@"1",@"76", nil];
    
        for (int i = 0; i<_sortData.count-1; i++) {
            
            for (int j = 0; j<_sortData.count-1; j++) {
                
                NSString *one = _sortData[j];
                NSString *next = _sortData[j+1];
                if ([one intValue] > [next intValue]) {
                    _sortData[j] = next;
                    _sortData[j+1] = one;
                }
                
            }
            
        }
    
    _sortAfter.text = [_sortData description];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
