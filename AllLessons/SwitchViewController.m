//
//  SwitchViewController.m
//  AllLessons
//
//  Created by dengbb on 15/8/21.
//  Copyright (c) 2015年 mohekeji. All rights reserved.
//

#import "SwitchViewController.h"

@interface SwitchViewController ()

@end

@implementation SwitchViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    int orderStatus = 9001;
    _statusValue.text = [NSString stringWithFormat:@"%d",orderStatus];
    
    // 选择语句的关键字：switch,case,break
    switch (orderStatus) {
            
        case 1001:

    _statusNameLabel.text = @"未出行1";
            break;
        case 2001:
    _statusNameLabel.text = @"未出行2";
            break;
        case 3001:
    _statusNameLabel.text = @"未出行3";
            break;
            
        case 4001:
    _statusNameLabel.text = @"未出行4";
            break;
            
        case 5001:
    _statusNameLabel.text = @"未出行5";
            break;
        case 6001:
    _statusNameLabel.text = @"未出行6";
            break;
        case 7001:
    _statusNameLabel.text = @"未出行7";
            break;
            
        case 8001:
    _statusNameLabel.text = @"未出行8";
            break;
            
        case 9001:
    _statusNameLabel.text = @"已出行";
            break;
        case 10001:
    _statusNameLabel.text = @"已取消";
            break;
        default:
    _statusNameLabel.text = @"未知错误";
            break;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
