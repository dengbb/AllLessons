
//
//  SecondViewController.m
//  AllLessons
//
//  Created by dengbb on 15/10/22.
//  Copyright © 2015年 mohekeji. All rights reserved.
//

#import "SecondViewController.h"
#import "ThirdViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

// 页面正在加载,还未显示
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_role == student) {

    }
    
    
    // UIControlEventTouchUpInside 为button的点击
    [_nextPageBtn addTarget:self action:@selector(toNext) forControlEvents:UIControlEventTouchUpInside];
    
    /*
     viewDidLoad
     加载页面元素
     例如：1.手写的控件
          2.一些逻辑
     总之为：页面出现之前的控件与数据的准备
     特点：只加载一次，接口刷新只能加载一遍
     跳回上个页面，页面被销毁
     */

}
// 页面将要出现
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // animated:动画
    /*
     viewWillAppear
     加载数据放这里，保证每次页面出现都会是最新的数据。
     问：页面控件与逻辑能放到这里吗？
     可以，但是浪费内存。除了特殊需求。

     */
}
// 页面已经出现
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    /*viewDidAppear
     
     页面加载完之后，把需要的事务放这里
     */
}


-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    /*
     viewDidDisappear
     页面已经消失
     */
    
}
-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    /*
     viewWillDisappear
     页面将要消失
     */
}


-(void)toNext{
    
    
    ThirdViewController *vc = [[ThirdViewController alloc] init];
    vc.valueStr = @"刚刚";
    vc.valueDic = @{@"key":@"明明"};
    vc.valueArr = @[@"洋洋",@{@"key":@"宁宁"}];
    
    
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
